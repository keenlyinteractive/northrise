<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip;';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function excerpt($limit) {

  $excerpt = explode(' ', get_the_excerpt(), $limit);

  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }

  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

  return $excerpt;
}


function favicon() {
  $template_url = get_template_directory_uri();

  $output='<link rel="manifest" href="' . $template_url . '/favicon/manifest.json">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#ff6600">
<meta name="application-name" content="Northrise University">
<link rel="apple-touch-icon" sizes="57x57" href="' . $template_url . '/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="' . $template_url . '/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="' . $template_url . '/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="' . $template_url . '/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="' . $template_url . '/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="' . $template_url . '/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="' . $template_url . '/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="' . $template_url . '/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="' . $template_url . '/favicon/apple-touch-icon-180x180.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="Northrise University">
<link rel="icon" type="image/png" sizes="228x228" href="' . $template_url . '/favicon/coast-228x228.png">
<link rel="yandex-tableau-widget" href="' . $template_url . '/favicon/yandex-browser-manifest.json">
<meta name="msapplication-TileColor" content="#ff6600">
<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
<meta name="msapplication-config" content="favicons/browserconfig.xml">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" href="' . $template_url . '/favicon/apple-touch-startup-image-320x460.png">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" href="' . $template_url . '/favicon/apple-touch-startup-image-640x920.png">
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="' . $template_url . '/favicon/apple-touch-startup-image-640x1096.png">
<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="' . $template_url . '/favicon/apple-touch-startup-image-750x1294.png">
<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3)" href="' . $template_url . '/favicon/apple-touch-startup-image-1182x2208.png">
<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3)" href="' . $template_url . '/favicon/apple-touch-startup-image-1242x2148.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)" href="' . $template_url . '/favicon/apple-touch-startup-image-748x1024.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)" href="' . $template_url . '/favicon/apple-touch-startup-image-768x1004.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)" href="' . $template_url . '/favicon/apple-touch-startup-image-1496x2048.png">
<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)" href="' . $template_url . '/favicon/apple-touch-startup-image-1536x2008.png">
<link rel="icon" type="image/png" sizes="32x32" href="' . $template_url . '/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="192x192" href="' . $template_url . '/favicon/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="16x16" href="' . $template_url . '/favicon/favicon-16x16.png">
<link rel="shortcut icon" href="' . $template_url . '/favicon/favicon.ico">';

  echo $output;
}
add_action('wp_head', __NAMESPACE__ . '\\favicon');
add_action('admin_head', __NAMESPACE__ . '\\favicon');


/*
 * => MODIFY QUERIES
 * ---------------------------------------------------------------------------*/

// Function to modify the main query object
function blog_query( $query ) {
  if ( $query->is_main_query() && is_home() ) {
    $query->set( 'offset', 1 );    // Offsets first post
  }
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\\blog_query' );