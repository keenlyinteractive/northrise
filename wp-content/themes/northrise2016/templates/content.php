<?php
	use Roots\Sage\Extras;
?>

<article <?php post_class(); ?>>
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-small' ); ?></a>

  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php echo wpautop(Extras\excerpt(25)); ?>

    <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
  </div>
</article>
