<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container flex">
    <?php get_search_form( true ); ?>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?php echo __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"></a>
    </div>

    <div class="header-right">
      <nav class="collapse navbar-collapse" role="navigation">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </nav>
    </div>
  </div>

  <div class="header-tag">
    <div class="container">
      <p>Educational, spiritual, and transformational development for the people of Zambia.</p>
    </div>
  </div>
</header>
<div class="header-spacing"></div>