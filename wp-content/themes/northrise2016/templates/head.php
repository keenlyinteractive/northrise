<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ['Lato:400,600,700']
      }
    });
  </script>
  <?php wp_head(); ?>
  <?php get_template_part('templates', 'favicon'); ?>
</head>
