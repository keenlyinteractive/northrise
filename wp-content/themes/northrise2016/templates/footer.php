<footer class="content-info">
  <div class="container">
    <div class="row">
    	<div class="col-sm-3">
    		<p><img src="<?php echo get_template_directory_uri(); ?>/dist/images/nui-logo-footer.png"></p>
    		<address>Address Line 1<br>
    		Address Line 2<br>
    		City, State, ZIP
    		</address>
    		<p><strong>T:</strong> (123) 456 7890<br>
    		<strong>E:</strong> info@northrise.com</p>
    	</div>

    	<div class="col-sm-3">
    		<h3>Navigation</h3>
    		<nav class="footer-nav" role="navigation">
    		  <?php
    		  if (has_nav_menu('primary_navigation')) :
    		    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'list-unstyled', 'depth' => '1']);
    		  endif;
    		  ?>
    		</nav>
    	</div>

    	<div class="col-sm-3">
    		<h3>News</h3>

    		<ul class="list-unstyled footer-news">
	    		<?php 
	    			// WP_Query arguments
	    			$args = array (
	    				'posts_per_page'         => '3',
	    			);

	    			// The Query
	    			$query = new WP_Query( $args );

	    			// The Loop
	    			if ( $query->have_posts() ) {
	    				while ( $query->have_posts() ) {
	    					$query->the_post();
	    					echo '<li>';
	    					echo '<a href="' . get_permalink( get_the_ID() ) . '">' . get_the_title() . '</a>';
	    					the_date( 'F j, Y', '<time>', '</time>' );
	    					echo '</li>';
	    				}
	    			} else {
	    				echo '<p>No news found</p>';
	    			}

	    			// Restore original Post Data
	    			wp_reset_postdata();
	  			?>
  			</ul>
    	</div>

    	<div class="col-sm-3">
    		<h3>Connect</h3>

    		<!-- Begin MailChimp Signup Form -->
    		<div id="mc_embed_signup">
	    		<form action="WAITING ON FORM ID FROM CLIENT" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <div id="mc_embed_signup_scroll">
			    		<div class="mc-field-group form-group">
			    			<input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Email *">
			    		</div>
		    			<div id="mce-responses" class="clear">
		    				<div class="response" id="mce-error-response" style="display:none"></div>
		    				<div class="response" id="mce-success-response" style="display:none"></div>
		    			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		  		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d17bd1de1420495d445cea9b2_37e8232177" tabindex="-1" value=""></div>
		  		    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary"></div>
	  		    </div>
	    		</form>
    		</div>
    		<!--End mc_embed_signup-->

    		<div class="social">
    			<a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    			<a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    			<a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    			<a href="#" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
  			</div>
    	</div>
    </div>

    <p class="copyrights">&copy; <?php echo date('Y'); ?> Northrise University. All Rights Reserved.</p>
  </div>
</footer>
