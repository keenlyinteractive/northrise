<?php

use Roots\Sage\Setup;

?>

<?php get_template_part('templates/page', 'header'); ?>

<div class="container sidebar-wrapper">
	<div class="row">
		<div class="content-area">
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'sage'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>

			<?php 
				// WP_Query arguments
				$args = array (
					'posts_per_page'         => '1',
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post(); ?>
						
						<?php the_post_thumbnail( 'post-large' ); ?>
						<h2><a href="<?php echo get_permalink( get_the_ID() ) ?>"><?php echo get_the_title() ?></a></h2>
						<?php the_excerpt(); ?>
						<a href="<?php echo get_the_permalink( get_the_ID() ) ?>" class="btn btn-primary">Read More</a>

					<?php }
				} else {
					echo '<p>No news found</p>';
				}

				// Restore original Post Data
				wp_reset_postdata();
			?>

			<div class="post-columns">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
				<?php endwhile; ?>
			</div>

			<?php the_posts_navigation(); ?>
		</div>

		<?php if (Setup\display_sidebar()) : ?>
		  <aside class="sidebar">
		    <?php echo do_shortcode('[fl_builder_insert_layout slug="blog-sidebar"]') ?>
		  </aside><!-- /.sidebar -->
		<?php endif; ?>
	</div>
</div>