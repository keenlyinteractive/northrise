<?php

use Roots\Sage\Setup;

?>

<div class="page-title">
	<div class="container">
	  <h1 class="entry-title"><?php the_title(); ?></h1>
	  <?php get_template_part('templates/entry-meta'); ?>
  </div>
</div>

<div class="container sidebar-wrapper">
	<div class="row">
		<div class="content-area">
			<?php get_template_part('templates/content-single', get_post_type()); ?>
		</div>

		<?php if (Setup\display_sidebar()) : ?>
		  <aside class="sidebar">
		    <?php echo do_shortcode('[fl_builder_insert_layout slug="blog-sidebar"]') ?>
		  </aside><!-- /.sidebar -->
		<?php endif; ?>
	</div>
</div>