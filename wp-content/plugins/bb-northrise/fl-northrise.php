<?php
/**
 * Plugin Name: Northrise Beaver Builder Addons
 * Description: Adds custom modules built for Northrise
 * Version: 1.0
 * Author: Stephen Greer
 */
define( 'FL_MODULE_IMAGE_LINK_DIR', plugin_dir_path( __FILE__ ) );
define( 'FL_MODULE_IMAGE_LINK_URL', plugins_url( '/', __FILE__ ) );

/**
 * Custom modules
 */
function fl_load_module_team_bio() {
	if ( class_exists( 'FLBuilder' ) ) {
    require_once 'team-bio/team-bio.php';
    require_once 'feature/feature.php';
    require_once 'sidebar-highlight/sidebar-highlight.php';
	}
}
add_action( 'init', 'fl_load_module_team_bio' );

/**
 * Custom fields
 */
function fl_team_bio_custom_field( $name, $value, $field ) {
    echo '<input type="text" class="text text-full" name="' . $name . '" value="' . $value . '" />';
}
add_action( 'fl_builder_control_my-custom-field', 'fl_team_bio_custom_field', 1, 3 );

/**
 * Custom field styles and scripts
 */
// function fl_team_bio_custom_field_assets() {
//     if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
//         wp_enqueue_style( 'my-custom-fields', FL_MODULE_IMAGE_LINK_URL . 'assets/css/fields.css', array(), '' );
//         wp_enqueue_script( 'my-custom-fields', FL_MODULE_IMAGE_LINK_URL . 'assets/js/fields.js', array(), '', true );
//     }
// }
// add_action( 'wp_enqueue_scripts', 'fl_team_bio_custom_field_assets' );