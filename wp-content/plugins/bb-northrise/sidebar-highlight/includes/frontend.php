<div class="<?php echo $module->get_classname(); ?>">
	<?php 
	
	// Image left
	$module->render_image('left'); 
	
	?>
	<div class="fl-sidebar-highlight-content">
		<?php 
		
		// Image above title
		?>

		<div class="fl-sidebar-highlight-photo">
			<?php
				$module->render_image('above-title'); 
			?>
		</div>
		
		<div class="fl-sidebar-highlight-text-wrap">
			<?php 

			// Title
			$module->render_title();
			
			// Text 
			$module->render_text();
			
			// Link CTA
			$module->render_link();
			
			// Button CTA 
			$module->render_button();
			
			?>
		</div> 
	</div> 
	<?php 
	
	// Image right
	$module->render_image('right'); 
	
	?>
</div>