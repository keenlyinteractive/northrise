<div class="<?php echo $module->get_classname(); ?>">
	<?php 
	
	// Image left
	$module->render_image('left'); 
	
	?>
	<div class="fl-feature-content">
		<?php 
		
		// Image above title
		?>

		<div class="fl-feature-photo">
			<?php
				$module->render_image('above-title'); 
		
				// Title
				$module->render_title();
			?>
		</div>
		
		<div class="fl-feature-text-wrap">
			<?php 
			
			// Text 
			$module->render_text();
			
			// Link CTA
			$module->render_link();
			
			// Button CTA 
			$module->render_button();
			
			?>
		</div> 
	</div> 
	<?php 
	
	// Image right
	$module->render_image('right'); 
	
	?>
</div>