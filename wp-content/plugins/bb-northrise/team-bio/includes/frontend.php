<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>

<div class="<?php echo $module->get_classname(); ?>">
	<?php 
	
	// Image left
	$module->render_image('left'); 
	
	?>
	<div class="fl-callout-content">
		<?php 
		
		// Image above title
		$module->render_image('above-title'); 
		
		// Title
		$module->render_title();
		
		// Image below title
		$module->render_image('below-title');
		
		?>
		<div class="fl-callout-text-wrap">
			<?php 
			
			// Text 
			$module->render_text();
			
			// Link CTA
			$module->render_link();
			
			?>
		</div> 
	</div> 
	<?php 
	
	// Image right
	$module->render_image('right'); 
	
	?>

	<div class="popout">
		<div class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">

		      <div class="modal-body">
		      	<button type="button" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
		      	<header class="row">
		      		<div class="col-sm-5">
					      <?php $module->render_image('above-title'); ?>
				      </div>
				      	
				      <div class="col-sm-7">
				      	<?php $module->render_title(); ?>
				      	<?php $module->render_text(); ?>
				      </div>
			      </header>
			      	
			      <?php
			      	$module->render_modal();
		      	?>
		      </div>

		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	</div>
</div>