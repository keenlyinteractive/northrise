<?php
  $chart_type = $settings->chart_type;
  $legend = $settings->legend;
?>

var ctx = document.getElementById("<?php echo $id ?>");

var myChart = new Chart(ctx, {
  type: '<?php echo $chart_type; ?>',
  data: {
    labels: [<?php echo $settings->labels; ?>],
    datasets: [{
      data: [<?php echo $settings->values; ?>],
      <?php if ($chart_type != "line") { ?> backgroundColor: [
        "#<?php if ($settings->color_1) { echo $settings->color_1; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_2) { echo $settings->color_2; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_3) { echo $settings->color_3; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_4) { echo $settings->color_4; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_5) { echo $settings->color_5; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_6) { echo $settings->color_6; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_7) { echo $settings->color_7; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_8) { echo $settings->color_8; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_9) { echo $settings->color_9; } else { echo "333333"; } ?>",
        "#<?php if ($settings->color_10) { echo $settings->color_10; } else { echo "333333"; } ?>",
      ], <?php } else { ?>
        backgroundColor: "rgba(0,0,0,0.1)",
        borderColor: "#<?php if ($settings->color_1) { echo $settings->color_1; } else { echo "333333"; } ?>",
      <?php } ?>
    }]
  },
  options: {
    legend: {
      display: <?php if ($legend === "show") {
        echo 'true';
      } else {
        echo 'false';
      } ?>
    }
  }
});