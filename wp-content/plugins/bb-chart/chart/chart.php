<?php

/**
 * @class FLTeamBioModule
 */
class FLChartModule extends FLBuilderModule {

 /** 
	* Constructor function for the module. You must pass the
	* name, description, dir and url in an array to the parent class.
	*
	* @method __construct
	*/  
	public function __construct() {
		parent::__construct(array(
		 'name'          => __('Chart', 'fl-builder'),
		 'description'   => __('Chart Modal.', 'fl-builder'),
		 'category'  => __('Advanced Modules', 'fl-builder'),
		));

		$this->add_js('chart-js', $this->url . 'js/Chart.min.js', array(), '', true);
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FLChartModule', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'fields'        => array(
					'labels'        => array(
						'type'            => 'text',
						'label'           => __('Labels', 'fl-builder'),
						'placeholder'   => __( '&quot;Green&quot;, &quot;Blue&quot;, &quot;Orange&quot;, &quot;Red&quot;', 'fl-builder' )
					),
					'values'        => array(
						'type'            => 'text',
						'label'           => __('Values', 'fl-builder'),
						'placeholder'   => __( '10, 20, 15, 13', 'fl-builder' )
					),
					'height'        => array(
						'type'            => 'text',
						'label'           => __('Chart Height', 'fl-builder'),
						'placeholder'   => __( '40' ),
						'description'   => __( 'Chart height as a percentage relative to its width', 'fl-builder' ),
					),
					'chart_type' => array(
				    'type'          => 'select',
				    'label'         => __( 'Chart Type', 'fl-builder' ),
				    'default'       => 'line',
				    'options'       => array(
			        'line'      => __( 'Line', 'fl-builder' ),
			        'bar'      => __( 'Bar', 'fl-builder' ),
			        'pie'      => __( 'Pie', 'fl-builder' ),
			        'doughnut'      => __( 'Doughnut', 'fl-builder' )
				    )
					),
				)
			)
		),
	),

	'style'       => array(
		'title'         => __('Style', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'fields'        => array(
					'legend' => array(
				    'type'          => 'select',
				    'label'         => __( 'Chart Legend', 'fl-builder' ),
				    'default'       => 'hide',
				    'options'       => array(
			        'hide'      => __( 'Hide', 'fl-builder' ),
			        'show'      => __( 'Show', 'fl-builder' )
				    )
					)
				)
			)
		)
	),

	'colors'       => array(
		'title'         => __('Colors', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'fields'        => array(
					'color_1'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 1', 'fl-builder' )
					),
					'color_2'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 2', 'fl-builder' )
					),
					'color_3'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 3', 'fl-builder' )
					),
					'color_4'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 4', 'fl-builder' )
					),
					'color_5'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 5', 'fl-builder' )
					),
					'color_6'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 6', 'fl-builder' )
					),
					'color_7'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 7', 'fl-builder' )
					),
					'color_8'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 8', 'fl-builder' )
					),
					'color_9'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 9', 'fl-builder' )
					),
					'color_10'    => array(
				    'type'          => 'color',
				    'label'         => __( 'Label Color 10', 'fl-builder' )
					)
				)
			)
		)
	)
));