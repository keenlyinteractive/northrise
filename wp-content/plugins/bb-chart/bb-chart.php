<?php
/**
 * Plugin Name: BB Chart
 * Description: Adds chart module to Beaver Builder
 * Version: 1.0
 * Author: Stephen Greer
 * Author URI: http://stephengreer.me/
 */
define( 'FL_MODULE_IMAGE_LINK_DIR', plugin_dir_path( __FILE__ ) );
define( 'FL_MODULE_IMAGE_LINK_URL', plugins_url( '/', __FILE__ ) );

/**
 * Custom modules
 */
function fl_load_custom_modules() {
	if ( class_exists( 'FLBuilder' ) ) {
    require_once 'chart/chart.php';
	}
}
add_action( 'init', 'fl_load_custom_modules' );